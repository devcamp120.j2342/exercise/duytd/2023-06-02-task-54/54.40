package com.devcamp.s5440.restapi.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class HTMLcontroller {
    @GetMapping("/html")
    public String getHTMLPage() {
        return "index.html"; // Trả về tên file HTML của bạn
    }
}
