package com.devcamp.s5440.restapi.Controllers;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@CrossOrigin
public class LuckyDice {
    @GetMapping("/greeting")
    public String greeting(@RequestParam String username, @RequestParam String firstname, @RequestParam String lastname) {
        Random random = new Random();
        int luckyNumber = random.nextInt(6) + 1; // Tạo số ngẫu nhiên từ 1 đến 6

        String message = "Xin chào, " + username + "! Số may mắn hôm nay của bạn là: " + luckyNumber;

        return message;
    }
}
